#ifndef FUNCTIONAL_H
#define FUNCTIONAL_H

constexpr auto compose() {
    return [](auto x){return x;};
}

constexpr auto compose(auto t, auto... args) {
    return [=](auto x){return (compose(args...))(t(x));};
}

constexpr auto lift(auto f) {
	return [=](auto x){return f();};
}

constexpr auto lift(auto f, auto x, auto... args) {
	return [=](auto t) {
		return lift([=](auto... args2){return f(x(t), args2...);}, args...)(t);
	};
}

#endif

