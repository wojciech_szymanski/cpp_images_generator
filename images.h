#ifndef IMAGES_H
#define IMAGES_H

#include <functional>
#include <cassert>
#include "color.h"
#include "coordinate.h"

typedef long double Fraction;

template<typename T> using Base_image = std::function<T(const Point)>;

using Region = Base_image<bool>;
using Image = Base_image<Color>;
using Blend = Base_image<Fraction>;

template<typename T> Base_image<T> constant(T t);
template<typename T> Base_image<T> rotate(Base_image<T> image, double phi);
template<typename T> Base_image<T> translate(Base_image<T> image, Vector v);
template<typename T> Base_image<T> scale(Base_image<T> image, double s);
template<typename T> Base_image<T> circle(Point q, double r, T inner, T outer);
template<typename T> Base_image<T> checker(double d, T this_way, T that_way);
template<typename T> Base_image<T> polar_checker(double d, int n, T this_way, T that_way);
template<typename T> Base_image<T> rings(Point q, double d, T this_way, T that_way);
template<typename T> Base_image<T> vertical_stripe(double d, T this_way, T that_way);

Image cond(Region region, Image this_way, Image that_way);
Image lerp(Blend blend, Image this_way, Image that_way);
Image darken(Image image, Blend blend);
Image lighten(Image image, Blend blend);

template<typename T>
Base_image<T> constant(T t) {
	return [=](const Point p){return t;};
}

template<typename T>
Base_image<T> rotate(Base_image<T> image, double phi) {
	return [=](const Point p) {
		Point x = p.is_polar ? p : to_polar(p);
		return image(Point(x.first, x.second - phi, true));
	};
}

template<typename T>
Base_image<T> translate(Base_image<T> image, Vector v) {
	return [=](const Point p) {
		Point x = p.is_polar ? from_polar(p) : p;
		return image(Point(x.first - v.first, x.second - v.second));
	};
}

template<typename T>
Base_image<T> scale(Base_image<T> image, double s) {
	return [=](const Point p) {
		Point x = p.is_polar ? from_polar(p) : p;
		return image(Point(x.first / s, x.second / s));
	};
}

template<typename T>
Base_image<T> circle(Point q, double r, T inner, T outer) {
	return [=](const Point p) {
		Point x = p.is_polar ? from_polar(p) : p;
		return distance(p, q) <= r ? inner : outer;
	};
}

template<typename T>
Base_image<T> checker(double d, T this_way, T that_way) {
	return [=](const Point p) {
		assert(d > 0);
		Point x = p.is_polar ? from_polar(p) : p;
		int64_t count_x = (x.first - (x.first >= 0 ? 0 : d)) / d;
		int64_t count_y = (x.second - (x.second >= 0 ? 0 : d)) / d;
		return (count_x % 2 + count_y % 2 + 2) % 2 ? that_way : this_way; 
	};
}

template<typename T>
Base_image<T> polar_checker(double d, int n, T this_way, T that_way) {
	return [=](const Point p) {
		assert(d > 0);
		Base_image<T> helper_image = checker(d, this_way, that_way);
		Point x = p.is_polar ? p : to_polar(p);
		Point x_t = Point(x.second * n * d / (2 * M_PI), x.first, false);
		return helper_image(x_t);
	};
}

template<typename T>
Base_image<T> rings(Point q, double d, T this_way, T that_way) {
	Base_image<T> helper_image = polar_checker(d, 1, this_way, that_way);
	Point x = q.is_polar ? from_polar(q) : q;
	return translate(helper_image, Vector(x.first, x.second));
}

template<typename T>
Base_image<T> vertical_stripe(double d, T this_way, T that_way) {
	return [=](const Point p) {
		Point x = p.is_polar ? from_polar(p) : p;
		return std::fabs(x.first) * 2 <= d ? this_way : that_way;
	};
}
	
#endif //IMAGES_H
